<?php

use app\models\Equipo;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Equipos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="equipo-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Añadir Equipo', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            

            'nomequipo',
            'director',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Equipo $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'nomequipo' => $model->nomequipo]);
                 }
            ],
        ],
    ]); ?>


</div>
