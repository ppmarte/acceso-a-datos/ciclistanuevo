<?php

use app\models\Maillot;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Maillots';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="maillot-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Añadir Maillot', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            

            'código',
            'tipo',
            'color',
            'premio',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Maillot $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'código' => $model->código]);
                 }
            ],
        ],
    ]); ?>


</div>
