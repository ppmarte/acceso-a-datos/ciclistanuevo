<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\data\ActiveDataProvider;
use app\models\Ciclista;
use yii\data\SqlDataProvider;
use app\models\Lleva;
use app\models\Etapa;
use app\models\Equipo;
use app\models\Puerto;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }
    
    //Consulta 1 
   
    //mediante active record
    public function actionConsulta1a(){
        //Se crea un provedor de datos
        $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find() ->select("edad")->distinct(),
        ]);
        
        //se renderiza la vista donde vamos a mostrar los datos
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['edad'],
            "titulo"=>"Consilta 1 con Active Record",
            "enunciado" => "Listar las edades de los ciclistas (sin repetidos)",
            "sql"=> "SELECT DISTINCT edad FROM ciclistas",
        ]);
    }
    
    //Mediante DAO
    public function actionConsulta1(){
        $dataProvider = new SqlDataProvider([
            'sql'=> 'SELECT DISTINCT edad FROM ciclista',
        ]);
        
        return $this->render('resultado',[
            "resultados"=> $dataProvider,
            "campos"=>['edad'],
            "titulo"=> "Consulta 1 con DAO",
            "enunciado"=>"Listas las edad de los ciclistas (sin duplicado)",
            "sql"=> "SELECT DISTINCT edad FROM ciclista",
        ]);
    }

    
    //Consulta 2
    
    //Mediante Active Redord
    
    public function actionConsulta2a(){
        $dataProvider = new ActiveDataProvider([
            'query'=>Ciclista::find() -> select("edad") ->  distinct()-> where("nomequipo = 'Artiach'"),
        ]);
        
        return $this->render('resultado',[
            "resultados"=>$dataProvider,
            "campos"=>['edad'],
            "titulo"=>"Consulta 2 con Active Record",
            "enunciado"=> "Listar las edades de los ciclistas de Artiach.",
            "sql"=> "SELECT DISTINCT edad FROM ciclista WHERE nomequipo='Artiach'",
        ]);
        
    }
    
    // Mediante DAO
    public function actionConsulta2(){
       $dataProvider= new sqlDataProvider([
            'sql'=> 'SELECT DISTINCT edad FROM ciclista
                WHERE nomequipo="artiach"',
        ]);
        return $this->render('resultado',[
            "resultados"=>$dataProvider,
            "campos"=> ['edad'],
            "titulo"=>"Consulta 2 con DAO",
            "enunciado"=>"Listar las edades de los ciclistas de Artiach.",
            "sql"=>"SELECT DISTINCT edad FROM ciclista WHERE nomequipo='Artiach'",
        ]);
    }
    
    
    //CONSULTA 3
    
    // Con Active Record
   public function actionConsulta3a(){
       $dataProvider = new ActiveDataProvider([
          'query'=> Ciclista::find()-> select("edad")-> distinct()-> where("nomequipo='Artiach' or nomequipo='Amore vita'"),
       ]);
       return $this->render('resultado',[
          "resultados"=>$dataProvider,
           "campos"=>['edad'],
           "titulo"=>"Consulta 3 con Active Record",
           "enunciado"=>"Listar las edades de los ciclistas de Artiach o de Amore Vita.",
           "sql"=>"SELECT DISTINCT edad FROM ciclista WHERE nomequipo='Artiach' OR nomequipo='Amore Vita'",
          
       ]);
   }
    
    // Con DAO
     public function actionConsulta3(){
       $dataProvider = new sqlDataProvider([
          'sql'=> 'SELECT DISTINCT edad FROM ciclista
           WHERE nomequipo="Artiach" OR nomequipo="Amore Vita"', 
       ]);
       return $this-> render('resultado',[
           "resultados"=>$dataProvider,
           "campos"=>['edad'],
           "titulo"=>"Consulta 3 con DAO",
           "enunciado"=>"Listar las edades de los ciclistas de Artiach o de Amore Vita.",
           "sql"=>"SELECT DISTINCT edad FROM ciclista WHERE nomequipo='Artiach' OR nomequipo='Amore Vita'",
          
       ]);
   }
    
    
    // CONSULTA 4
   
    // Con Active Record
    public function actionConsulta4a(){
       $dataProvider = new ActiveDataProvider([
            'query'=>Ciclista::find() -> select("edad") ->  distinct()->where("edad < 25 or edad>30"),
        ]);
        
        return $this->render('resultado',[
            "resultados"=>$dataProvider,
            "campos"=>['edad'],
            "titulo"=>"Consulta 4 con Active Record",
            "enunciado"=> "Listar los dorsales de los ciclistas cuya edad sea menor que 25 o mayor que 30",
            "sql"=> "SELECT DISTINCT edad FROM ciclista WHERE edad <25 OR edad>30",
        ]);
   }
    
    // Con DAO
     public function actionConsulta4(){
        $dataProvider = new sqlDataProvider([
          'sql'=> 'SELECT DISTINCT edad FROM ciclista
           WHERE edad <25 OR edad>30', 
       ]);
       return $this-> render('resultado',[
           "resultados"=>$dataProvider,
           "campos"=>['edad'],
           "titulo"=>"Consulta 4 con DAO",
           "enunciado"=>"listar las edades de los ciclistas de Artiach o de Amore Vita.",
           "sql"=>"SELECT DISTINCT edad FROM ciclista WHERE WHERE edad <25 OR edad>30",
          
       ]);
   }
    
    
    // CONSULTA 5
    // Con Active Record
    public function actionConsulta5a(){
       $dataProvider = new ActiveDataProvider([
            'query'=>Ciclista::find() -> select("edad") ->  distinct()->where("edad between 28 and 32"),
        ]);
        
        return $this->render('resultado',[
            "resultados"=>$dataProvider,
            "campos"=>['edad'],
            "titulo"=>"Consulta 5 con Active Record",
            "enunciado"=> "listar los dorsales de los ciclistas cuya edad esté entre 28 y 32 y además que sólo sean de Banesto",
            "sql"=> "SELECT DISTINCT edad FROM ciclista WHERE edad between 28 AND 32",
        ]);
   }
    
    // Con DAO
     public function actionConsulta5(){
       $dataProvider = new sqlDataProvider([
          'sql'=> 'SELECT DISTINCT edad FROM ciclista
           WHERE edad  between 28 AND 32', 
       ]);
       return $this-> render('resultado',[
           "resultados"=>$dataProvider,
           "campos"=>['edad'],
           "titulo"=>"Consulta 4 con DAO",
           "enunciado"=>"listar los dorsales de los ciclistas cuya edad esté entre 28 y 32 y además que sólo sean de Banesto",
           "sql"=>"SELECT DISTINCT edad FROM ciclista WHERE edad between 28 AND 32",
          
       ]);
   }
    
    // CONSULTA 6
    // Con Active Record
    public function actionConsulta6a(){
       $dataProvider = new ActiveDataProvider([
            'query'=>Ciclista::find() -> select("nombre") -> where("char_length(nombre)>8"),
        ]);
        
        return $this->render('resultado',[
            "resultados"=>$dataProvider,
            "campos"=>['nombre'],
            "titulo"=>"Consulta 6 con Active Record",
            "enunciado"=> "Indícame el nombre de los ciclistas que el número de caracteres del nombre sea mayor que 8",
            "sql"=> "SELECT DISTINCT edad FROM ciclista WHERE char_length(nombre) > 8",
        ]);
   }
    
    // Con DAO
     public function actionConsulta6(){
       $dataProvider = new sqlDataProvider([
          'sql'=> 'SELECT DISTINCT nombre FROM ciclista
           WHERE char_length(nombre) > 8', 
       ]);
       return $this-> render('resultado',[
           "resultados"=>$dataProvider,
           "campos"=>['nombre'],
           "titulo"=>"Consulta 6 con DAO",
           "enunciado"=>"Indícame el nombre de los ciclistas que el número de caracteres del nombre sea mayor que 8",
           "sql"=>"SELECT DISTINCT edad FROM ciclista WHERE char_length(nombre) > 8",
          
       ]);
   }
    
    // CONSULTA 7
    // Con Active Record
    public function actionConsulta7a(){
       $dataProvider = new ActiveDataProvider([
            'query'=>Ciclista::find() -> select("nombre, dorsal, UPPER(nombre) mayuscula"),
        ]);
        
        return $this->render('resultado',[
            "resultados"=>$dataProvider,
            "campos"=>['nombre','dorsal', 'mayuscula'],
            "titulo"=>"Consulta 7 con Active Record",
            "enunciado"=> "lístame el nombre y el dorsal de todos los ciclistas 
           mostrando un campo nuevo denominado nombre mayúsculas que debe mostrar el nombre en mayúsculas.",
            "sql"=> "SELECT DISTINCT nombre, UPPER(nombre) as 'nombre mayúscula' FROM ciclista ",
        ]);
   }
    
    // Con DAO
     public function actionConsulta7(){
       $dataProvider = new sqlDataProvider([
          'sql'=> 'SELECT DISTINCT nombre, dorsal, UPPER(nombre) "Nombre en Mayúsculas" FROM ciclista', 
       ]);
       return $this-> render('resultado',[
           "resultados"=>$dataProvider,
           "campos"=>['nombre', 'dorsal', 'Nombre en Mayúsculas'],
           "titulo"=>"Consulta 7 con DAO",
           "enunciado"=>"lístame el nombre y el dorsal de todos los ciclistas 
           mostrando un campo nuevo denominado nombre mayúsculas que debe mostrar el nombre en mayúsculas.",
           "sql"=>"SELECT DISTINCT nombre, UPPER(nombre) as 'Nombre Mayúscula' FROM ciclista",
          
       ]);
   }
    
    
    // CONSULTA 8
    // Con Active Record
    public function actionConsulta8a(){
       $dataProvider = new ActiveDataProvider([
            'query'=>Lleva::find() -> select("dorsal") ->  distinct()->where("código='MGE'"),
        ]);
        
        return $this->render('resultado',[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 8 con Active Record",
            "enunciado"=> "Listar todos los ciclistas que han ganado el maillot MGE (amarillo) en alguna etapa.",
            "sql"=> "SELECT DISTINCT dorsal FROM ciclista WHERE código='MGE'",
        ]);
   }
    
    // Con DAO
     public function actionConsulta8(){
       $dataProvider = new sqlDataProvider([
          'sql'=> 'SELECT DISTINCT dorsal FROM lleva WHERE código="MGE"', 
       ]);
       return $this-> render('resultado',[
           "resultados"=>$dataProvider,
           "campos"=>['dorsal'],
           "titulo"=>"Consulta 8 con DAO",
           "enunciado"=>"Listar todos los ciclistas que han ganado el maillot MGE (amarillo) en alguna etapa.",
           "sql"=>"SELECT DISTINCT dorsal FROM ciclista WHERE código='MGE'",
          
       ]);
   }
    
    // CONSULTA 9
    // Con Active Record
    public function actionConsulta9a(){
       $dataProvider = new ActiveDataProvider([
            'query'=>Puerto::find() -> select("nompuerto") ->  distinct()->where("altura < 1500"),
        ]);
        
        return $this->render('resultado',[
            "resultados"=>$dataProvider,
            "campos"=>['nompuerto'],
            "titulo"=>"Consulta 9 con Active Record",
            "enunciado"=> "Listar el nombre de los puertos cuya altura sea mayor de 1500.",
            "sql"=> "SELECT DISTINCT nompuerto FROM puerto WHERE altura < 1500",
        ]);
   }
    
    // Con DAO
     public function actionConsulta9(){
       $dataProvider = new sqlDataProvider([
          'sql'=> 'SELECT DISTINCT nompuerto FROM puerto
           WHERE altura < 1500', 
       ]);
       return $this-> render('resultado',[
           "resultados"=>$dataProvider,
           "campos"=>['nompuerto'],
           "titulo"=>"Consulta 9 con DAO",
           "enunciado"=>"Listar el nombre de los puertos cuya altura sea mayor de 1500.",
           "sql"=>"SELECT DISTINCT nompuerto FROM puerto WHERE altura < 15000",
          
       ]);
   }
    
    
    // CONSULTA 10
    // Con Active Record
    public function actionConsulta10a(){
       $dataProvider = new ActiveDataProvider([
            'query'=>Puerto::find() -> select("dorsal") ->  distinct()->where("pendiente > 9 OR altura between 1800 AND 3000"),
        ]);
        
        return $this->render('resultado',[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 10 con Active Record",
            "enunciado"=> "Listar el dorsal de los ciclistas que hayan ganado algun puerto 
           cuya pendiente sea mayor que 8 o cuya altura esté entre 1800 y 3000.",
            "sql"=> "SELECT DISTINCT dorsal from puerto WHERE pendiente > 8 OR altura BETWEEN 1800 AND 3000",
        ]);
   }
    
    // Con DAO
     public function actionConsulta10(){
       $dataProvider = new sqlDataProvider([
          'sql'=> 'SELECT DISTINCT dorsal FROM puerto WHERE pendiente > 8  OR altura BETWEEN 1800 AND 3000', 
       ]);
       return $this-> render('resultado',[
           "resultados"=>$dataProvider,
           "campos"=>['dorsal'],
           "titulo"=>"Consulta 10 con DAO",
           "enunciado"=>"Listar el dorsal de los ciclistas que hayan ganado algun puerto 
           cuya pendiente sea mayor que 8 o cuya altura esté entre 1800 y 3000.",
           "sql"=>"SELECT DISTINCT dorsal from puerto WHERE pendiente > 8  OR altura BETWEEN 1800 AND 3000",
          
       ]);
   }
    
    
    // CONSULTA 11
   // Con Active Record
    public function actionConsulta11a(){
       $dataProvider = new ActiveDataProvider([
            'query'=>Puerto::find() -> select("dorsal") ->  distinct()->where("pendiente > 8  AND altura BETWEEN 1800 AND 3000"),
        ]);
        
        return $this->render('resultado',[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 11 con Active Record",
            "enunciado"=> "Listar el dorsal de los ciclistas que hayan ganado algún puerto 
           cuya pendiente sea mayor que 8 y cuya altura esté entre 1800 y 3000.",
            "sql"=> "SELECT DISTINCT dorsal from puerto WHERE pendiente > 8  AND altura BETWEEN 1800 AND 3000",
        ]);
   }
    
    // Con DAO
     public function actionConsulta11(){
       $dataProvider = new sqlDataProvider([
          'sql'=> 'SELECT DISTINCT dorsal from puerto WHERE pendiente > 8  and altura between 1800 AND 3000', 
       ]);
       return $this-> render('resultado',[
           "resultados"=>$dataProvider,
           "campos"=>['dorsal'],
           "titulo"=>"Consulta 11 con DAO",
           "enunciado"=>"Listar el dorsal de los ciclistas que hayan ganado algún puerto 
           cuya pendiente sea mayor que 8 y cuya altura esté entre 1800 y 3000.",
           "sql"=>"SELECT DISTINCT dorsal from puerto WHERE pendiente > 8  AND altura BETWEEN 1800 AND 3000",
          
       ]);
   }
    
    
    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
}
